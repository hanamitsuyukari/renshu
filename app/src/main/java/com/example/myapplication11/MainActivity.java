package com.example.myapplication11;

import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Button start = findViewById(R.id.button);
        Button stop =  findViewById(R.id.button2);
        Button submit = findViewById(R.id.button3);
        Button write = findViewById(R.id.button4);
        Button read = findViewById(R.id.button5);
        Button writeP = findViewById(R.id.button6);
        start.setOnClickListener(this);
        stop.setOnClickListener(this);
        submit.setOnClickListener(this);
        write.setOnClickListener(this);
        read.setOnClickListener(this);
        writeP.setOnClickListener(this);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        String message = "";

        //noinspection SimplifiableIfStatement
        switch (id){
            case R.id.action_settings:
                message = "settingを選択";
                break;
            case R.id.action_item1:
                message = "item1を選択";
                break;
            case R.id.action_item2:
                message = "item2を選択";
                break;
            case R.id.action_item4:
                message = "item4を選択";
                break;
            case R.id.action_item5:
                message = "item5を選択";
                break;
        }

        Toast ts = Toast.makeText(this,message,Toast.LENGTH_LONG);
        ts.show();

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        TextView t = (TextView)findViewById(R.id.textView);
        EditText e = (EditText)findViewById(R.id.editText2);
        CharSequence moji = e.getText();

        if(v != null){
            switch (v.getId()){
                case R.id.button:
                    t.setText("こんにちは");
                    break;
                case R.id.button2:
                    t.setText("ありがとう");
                    break;
                case R.id.button3:
                    Toast ts = Toast.makeText(this,moji,Toast.LENGTH_LONG);
                    ts.show();
                case R.id.button4:
                    writeFile();
                    break;
                case R.id.button5:
                    readFile();
                    break;
                case R.id.button6:
                    writePref();

                }
        }

    }

    private void writeFile(){
        EditText edit = (EditText)findViewById(R.id.editText2);
        String st = edit.getText().toString();
        String result = "書き込み完了";

        if(edit.length() != 0){
            try{
                FileOutputStream fos = openFileOutput("file.txt",MODE_APPEND);
                BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));

                bw.write(st + "\n");
                bw.close();
            }catch (Exception e){
                result = "書き込み失敗";
            }finally {
                Toast ts = Toast.makeText(this,result,Toast.LENGTH_LONG);
                ts.show();
                edit.setText("");
            }
        }


    }

    private void readFile(){
        String result = "";
        try{
            FileInputStream fis = openFileInput("file.txt");
            BufferedReader br = new BufferedReader(new InputStreamReader(fis));
            String line;
            while((line = br.readLine()) != null){
                result += line + ",";
            }
            br.close();
        }catch(Exception e){
            result ="読み込みできません";
        }finally {
            Toast ts = Toast.makeText(this,result,Toast.LENGTH_LONG);
            ts.show();
        }


    }

    private void writePref(){
        EditText edit = (EditText)findViewById(R.id.editText2);
        String st = edit.getText().toString();
        String result = "書き込み完了";

        SharedPreferences sp = getSharedPreferences("fileP",MODE_APPEND);
        SharedPreferences.Editor se = sp.edit();

        if(edit.length() == 0){
            AlertDialog.Builder ab =  new AlertDialog.Builder(this);
            ab.setTitle("エラー");
            ab.setMessage("文字が入力されていません");
            ab.show();
            return;
        }else{
            se.putString("input",st);
            if(se.commit() == false) {
                result = "書き込み失敗";
            }
        }

        Toast ts = Toast.makeText(this,result,Toast.LENGTH_SHORT);
        ts.show();


    }
}
